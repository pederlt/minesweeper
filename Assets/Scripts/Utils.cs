﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Utils {

	public static void PrintGameObjectList(String msg, List<Tile> li) {
		Debug.Log(msg + "(" + li.Count + "):" + GetListString(li));
	}

	public static void PrintGameObjectList(List<Tile> li) {
		Debug.Log("List(" + li.Count + "):" + GetListString(li));
	}

	private static String GetListString(List<Tile> li) {
		string s = "";
		foreach (Tile g in li) {
			s += "[" + (int)g.transform.position.x + "," + (int)g.transform.position.y + "] ";
		}
		return s;
	}

}
