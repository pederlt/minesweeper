﻿using System;
using System.Collections.Generic;
using UnityEngine;

public class Board {

	private Tiles grid;

	public Board(int width, int height, GameManager boardScript, UnityEngine.Object prefab) {
		grid = new Tiles(width, height);
		InitializeBoard(prefab, boardScript);
	}

	public bool IsBlank(Tile g) {
		return GetAdjacentMines(g).Count == 0 && !g.IsMine();
	}

	public bool ContainsNode(Tile g) {
		return grid.Contains(g);
	}

	public List<Tile> GetAdjacentUnopenedEmptyNodes(Tile origin) {
		List<Tile> ret = new List<Tile>();
		foreach (Tile tile in GetAdjacentNodes(origin)) {
			if (!tile.isOpen && !tile.isFlagged) {
				ret.Add(tile);
			}
		}
		Utils.PrintGameObjectList("GetAdjacentUnopenedEmptyNodes", ret);
		return ret;
	}

	public List<Tile> GetAdjacentNodes(Tile t) {
		return grid.GetAdjacentNodes(t);
	}

	public void InitializeBoard(UnityEngine.Object prefab, GameManager boardScript) {
		for (int x = 0; x < grid.width; x++) {
			for (int y = 0; y < grid.height; y++) {
				GameObject tile = UnityEngine.Object.Instantiate(prefab, new Vector3(x, y, 0), Quaternion.identity) as GameObject;
				tile.GetComponent<Tile>().setBoardScript(boardScript);
				grid.SetNode(x, y, tile.GetComponent<Tile>());
			}
		}
	}

	public void Destroy() {
		foreach (GameObject g in GameObject.FindGameObjectsWithTag("Tile")) {
			GameObject.Destroy(g);
		}
	}

	public void NodeLeftClicked(Tile origin) {
		int originAdjacentMines = GetAdjacentMines(origin).Count;
		bool originWasOpened = origin.OpenClick(originAdjacentMines);

		if (originWasOpened) { //TOOD untested
			if (originAdjacentMines == 0) {
				foreach (Tile fieldNode in FieldFinder.GetFieldNodes(this, origin)) {
					fieldNode.Reveal(GetAdjacentMines(fieldNode).Count);
				}
			} else {
				Debug.Log("Node was opened & has " + originAdjacentMines + " adjacent mines.");
			}
		} else if (origin.isFlagged) { //Click on flag
			Debug.Log("Can't open flagged");
		} else { //Already opened
			List<Tile> toOpen = GetProximityNodes(origin);
			List<Tile> blanks = new List<Tile>();
			foreach (Tile proximity in toOpen) {
				if(IsBlank(proximity)) {
					Debug.Log("Proximity opened blank");
					blanks.Add(proximity);
				} else {
					Debug.Log("Proximity opened" + proximity.toStringExt());
					proximity.Reveal(GetAdjacentMines(proximity).Count);
				}
			}
			Utils.PrintGameObjectList("Found blanks", blanks);
			List<Tile> revealed = new List<Tile>();
			foreach(Tile blank in blanks) {
				foreach (Tile fieldNode in FieldFinder.GetFieldNodes(this, blank)) {
					if (!fieldNode.isOpen) {
						Debug.Log("Proximity field-reveal:" + fieldNode.toStringExt());
						fieldNode.Reveal(GetAdjacentMines(fieldNode).Count);
						revealed.Add(fieldNode);
					} else {
						Debug.Log("Proximity already opened." + fieldNode.toStringExt());
					}
					
				}
			}
			foreach(Tile blank in blanks) {
				if (!blank.isOpen) {
					Debug.LogError("Didn't open all blank nodes. Opening now. " + blank.toStringExt());
					blank.Reveal(GetAdjacentMines(blank).Count);
				}
			}
		}
	}

	private List<Tile> GetProximityNodes(Tile origin) {
		List<Tile> toOpen = new List<Tile>();
		int adjacentMines = GetAdjacentMines(origin).Count;
		int adjacentFlags = GetAdjacentFlags(origin).Count;
		if (adjacentMines == adjacentFlags && origin.isOpen == true) {
			foreach (Tile node in GetAdjacentUnopenedEmptyNodes(origin)) {
				toOpen.Add(node);
			} 
		} else {
			Debug.Log("Proximity search: Minecount != Flagcount. " + origin.toStringExt());
		}
		Utils.PrintGameObjectList("Proximity nodes", toOpen);
		return toOpen;
	}

	public void RevealMines() {
		foreach (Tile mine in GetMines()) {
			mine.TryRevealMine();
		}
	}

	public void NodeRightClicked(Tile tile) {
		tile.FlagClick();
	}

	public List<Tile> GetMines() {
		List<Tile> mines = new List<Tile>();
		foreach (Tile tile in grid.AsList()) {
			if (tile.IsMine()) {
				mines.Add(tile);
			}
		}
		return mines;
	}

	public int GetClosedEmptyNodeCount() {
		int closedNodes = 0;
		List<Tile> li = grid.AsList();
		foreach (Tile g in li) {
			if (!g.isOpen && !g.IsMine()) {
				closedNodes++;
			}
		}
		return closedNodes;
	}

	public List<Tile> GetAdjacentMines(Tile t) {
		List<Tile> nodes = new List<Tile>();
		foreach (Tile g in grid.GetAdjacentNodes(t)) {
			if (g.GetComponent<Tile>().IsMine() == true) {
				nodes.Add(g);
			}
		}
		return nodes;
	}

	public List<Tile> GetAdjacentFlags(Tile t) {
		List<Tile> nodes = new List<Tile>();
		foreach (Tile g in grid.GetAdjacentNodes(t)) {
			if (g.isFlagged == true) {
				nodes.Add(g);
			}
		}
		return nodes;
	}


}
