﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;

public class FieldFinder {

	/*
		1. Uncover, legg i visited
		2. Hent adjacent nodes.
		3. 
			1. Blank skal utforskes videre -> unvisited
			2. Not blank skal åpnes, og legges i visited
		4. Fjern blank fra unvisited
		5. Hent Adjacent nodes.
		6. 
			1. Blank som ikke er i visited -> unvisited
			2. Not blank som ikke er i visited, åpnes -> legges i visited.
		7. repeat til unvisited er tom.
	*/

	public static List<Tile> GetFieldNodes(Board grid, Tile origin) {
		if (!grid.IsBlank(origin)) {
			Debug.LogWarning("Skal ikke finne fields for non-blank nodes");
			return new List<Tile>();
		}

		List<Tile> toOpen = new List<Tile>();
		Dictionary<String, Tile> unvisited = new Dictionary<string, Tile>();
		Dictionary<String, Tile> visited = new Dictionary<string, Tile>();
		visited.Add(GetId(origin), origin);

		ProcessAdjacentNodes(grid, origin, toOpen, visited, unvisited);

		while (unvisited.Count > 0) {
			Tile node = popElement(unvisited);
			String key = GetId(node);
			if (!visited.ContainsKey(key)) {
				visited.Add(key, node);
			}
			ProcessAdjacentNodes(grid, node, toOpen, visited, unvisited);
		}

		Utils.PrintGameObjectList("Field search results:", toOpen);
		return toOpen;
	}

	private static void ProcessAdjacentNodes(Board grid,
		Tile origin, 
		List<Tile> toOpen,
		Dictionary<String, Tile> visited,
		Dictionary<String, Tile> unvisited) {
		foreach (Tile node in grid.GetAdjacentNodes(origin)) {
			String key = GetId(node);
			if (!toOpen.Contains(node)) {
				if (grid.IsBlank(node) && !visited.ContainsKey(key)) { // blank node - legg til adjacent nodes for check
					AddIfMissing(node, unvisited);
				} else {
					AddIfMissing(node, visited);
				}
				toOpen.Add(node);
			}
		}
	}

	private static void AddIfMissing(Tile g, Dictionary<String, Tile> d) {
		String key = GetId(g);
		if (!d.ContainsKey(key)) {
			d.Add(key, g);
		}
	}

	private static void TryAdd(Tile g, List<Tile> li) {
		if(!li.Contains(g)) {
			li.Add(g);
		}
	}

	private static Tile popElement(Dictionary<String, Tile> d) {
		Tile node = GetRandomUnopenedNode(d);
		String key = GetId(node);
		d.Remove(key);
		return node;
	}

	private static String GetId(Tile t) {
		return "" + (int)t.transform.position.x + "," + (int)t.transform.position.y;
	}

	private static Tile GetRandomUnopenedNode(Dictionary<String, Tile> d) {
		Tile node = null;
		String key = RandomUnopendKey(d);
		d.TryGetValue(key, out node);
		return node;
	}

	private static String RandomUnopendKey(Dictionary<String, Tile> d) {
		String ret = null;
		foreach (String key in d.Keys) {
			ret = key;
		}
		return ret;
	}




	

}
