﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Tile : MonoBehaviour {

	public bool isOpen;
	public bool isFlagged;
	private bool isMine;
	public Sprite[] emptyTexture;
    public Sprite mineTexture;
	public Sprite flagTexture;
	public Sprite defaultTexture;
	public Sprite badMark;
	GameManager boardScript;

	private void Awake() {
		isOpen = false;
		isFlagged = false;
		isMine = UnityEngine.Random.value < 0.15;
	}
	// Use this for initialization
	void Start () {
		//Debug.Log("tile start");
	}

	// Update is called once per frame
	void Update () {
	}

	public bool IsMine() {
		return isMine;
	}

	public String toString() {
		return GetXpos() + "," + GetYpos();
	}

	public String toStringExt() {
		return GetXpos() + "," + GetYpos() + " - flag:" + isFlagged + ", mine:" + isMine + ", isOpen:" + isOpen;
	}

	public void setBoardScript(GameManager boardScript) {
		this.boardScript = boardScript;
	}

	public void TryRevealMine() {
		if (isMine && !isFlagged) {
			GetComponent<SpriteRenderer>().sprite = mineTexture;
		}
		if (!isMine && isFlagged) {
			GetComponent<SpriteRenderer>().sprite = mineTexture; // TODO 
		}
	}


	public void Reveal(int adjacentMines) {
		if(isOpen == true) {
			Debug.LogWarning("Revealing already opened:" + toStringExt());
		}
		isOpen = true;
		if (isMine) {
			GetComponent<SpriteRenderer>().sprite = mineTexture;
			boardScript.Gameover();
		} else {
			GetComponent<SpriteRenderer>().sprite = emptyTexture[adjacentMines];
		}
	}

	public void FlagClick() {
		if (CanBeFlagged()) {
			if (isFlagged) {
				GetComponent<SpriteRenderer>().sprite = defaultTexture;
			} else {
				GetComponent<SpriteRenderer>().sprite = flagTexture;
			}
			isFlagged = !isFlagged;
		}
	}

	public bool OpenClick(int adjacentMines) {
		if (CanBeOpened()) {
			Reveal(adjacentMines);
			return true;
		} 
		//Debug.Log("cannot be opened");
		return false;
	}

	private int GetXpos() {
		return (int)GetComponent<Transform>().transform.position.x;
	}

	private int GetYpos() {
		return (int)GetComponent<Transform>().transform.position.y;
	}

	private bool CanBeOpened() {
		return !isOpen && !isFlagged;
	}

	private bool CanBeFlagged() {
		return !isOpen;
	}
}
