﻿using System;
using UnityEngine;

public class GameManager : MonoBehaviour {

	public int width;
	public int height;
	public Board board;
	public GameObject tilePrefab;

	private bool first;
	private bool gameover;

	// Use this for initializationnoen
	void Start() {
		InitGame();
	}

	public void Gameover() {
		Debug.Log("gameover");
		gameover = true;
		board.RevealMines();
	}

	// Update is called once per frame
	void Update () {
		RunOnce();
		HandleGameInput();
		if (GameWon()) {
			Win();
		}
	}

	private void RunOnce() {
		if (first) {
			Utils.PrintGameObjectList("Mines", board.GetMines());
			first = false;
		}
	}

	private bool GameWon() {
		return !gameover && board.GetClosedEmptyNodeCount() == 0;
	}

	private void Win() {
		Debug.Log("Won!");
		gameover = true;
		board.RevealMines();
	}

	private void HandleGameInput() {
		if (Input.GetMouseButtonDown(0) && gameover == false) {
			LeftClick();
		}
		if (Input.GetMouseButtonDown(1) && gameover == false) {
			RightClick();
		}
		if (Input.GetKeyUp("r")) {
			RestartGame();
		}
	}

	private void InitGame() {
		Debug.Log("game start");
		board = new Board(9, 9, this, tilePrefab);
		gameover = false;
		first = true;
	}

	private void RestartGame() {
		Debug.Log("game restart");
		board.Destroy();
		InitGame();
	}

	private void LeftClick() {
		GameObject clickedObject = GetClickTarget();
		if (clickedObject != null && board.ContainsNode(clickedObject.GetComponent<Tile>())) {
			board.NodeLeftClicked(clickedObject.GetComponent<Tile>());
		} else {
			Debug.Log("TODO non - tile hit");
		}
	}

	private void RightClick() {
		GameObject clickedObject = GetClickTarget();
		if (clickedObject != null && board.ContainsNode(clickedObject.GetComponent<Tile>())) {
			board.NodeRightClicked(clickedObject.GetComponent<Tile>());
		} else {
			Debug.Log("TODO non - tile hit");
		}
	}

	private GameObject GetClickTarget() { 
		try {
			Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
			RaycastHit2D hit = Physics2D.Raycast(ray.origin, -Vector2.up);
			return hit.transform.gameObject; //TODO set tag
		} catch (Exception e) {
			Debug.Log("Could not get object clicked on: " + e.Message);
			return null;
		}
		
	}
	
}
