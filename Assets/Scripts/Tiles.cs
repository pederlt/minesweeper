﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Tiles {

	private Tile[,] tiles;
	public int height;
	public int width;

	public Tiles(int width, int height) {
		tiles = new Tile[width, height];
		this.width = width;
		this.height = height;
	}

	public void SetNode(int x, int y, Tile g) {
		tiles[x, y] = g;
	}

	public Tile GetScript(int x, int y) {
		return GetNode(x, y).GetComponent<Tile>();
	}

	public Tile GetNode(int x, int y) {
		if (WithinBoard(x, y)) {
			return tiles[x, y];
		} else {
			throw new NotImplementedException("Getnode returnerte null");
		}
	}

	public List<Tile> AsList() {
		List<Tile> li = new List<Tile>();
		for (int x = 0; x < width; x++) {
			for (int y = 0; y < height; y++) {
				li.Add(tiles[x, y]);
			}
		}
		return li;
	}

	public bool Contains(Tile g) {
		if(g == null) {
			Debug.LogError("Contains on null.");
			return false;
		}
		for (int i = 0; i < width; i++) {
			for (int j = 0; j < height; j++) {
				if (g.Equals(tiles[i, j])) {
					return true;
				}
			}
		}
		return false;
	}

	public List<Tile> GetAdjacentNodes(Tile t) {
		return GetAdjacentNodes((int)t.transform.position.x, (int)t.transform.position.y);
	}

	public List<Tile> GetAdjacentNodes(int x, int y) {
		List<Tile> adjacentNodes = new List<Tile>();
		int xMin = x - 1;
		int yMin = y - 1;
		int xMax = x + 1;
		int yMax = y + 1;

		for (int ix = xMin; ix <= xMax; ix++) {
			for (int iy = yMin; iy <= yMax; iy++) {
				bool isCenterTile = (ix == x && iy == y);
				if (WithinBoard(ix, iy) && !isCenterTile) {
					adjacentNodes.Add(tiles[ix, iy]);
				}
			}
		}
		return adjacentNodes;
	}

	public bool WithinBoard(int x, int y) {
		bool withinX = x >= 0 && x < width;
		bool withinY = y >= 0 && y < height;

		return withinX && withinY;
	}
}
